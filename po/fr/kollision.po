# translation of kollision.po to Français
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Joëlle Cornavin <jcornavi@club-internet.fr>, 2008.
# Sébastien Renard <Sebastien.Renard@digitalfox.org>, 2008.
# Guillaume Pujol <guill.p@gmail.com>, 2010.
# SPDX-FileCopyrightText: 2013, 2020, 2024 Xavier Besnard <xavier.besnard@kde.org>
# Simon Depiets <sdepiets@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: kollision\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-03-10 00:40+0000\n"
"PO-Revision-Date: 2024-05-08 18:13+0200\n"
"Last-Translator: Xavier Besnard <xavier.besnard@kde.org>\n"
"Language-Team: French <French <kde-francophone@kde.org>>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.08.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Joëlle Cornavin, Simon Depiets"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "jcornavi@club-internet.fr, sdepiets@gmail.com"

#. i18n: ectx: label, entry (IncreaseBallSize), group (game_options)
#: kollision.kcfg:9
#, kde-format
msgid "Whether automatically increasing ball size should be applied."
msgstr "Définit si la taille des balles doit augmenter automatiquement."

#. i18n: ectx: label, entry (EnableSounds), group (sounds)
#: kollision.kcfg:15
#, kde-format
msgid "Whether sound effects should be played."
msgstr "Définit si les effets sonores doivent ou non être joués."

#. i18n: ectx: Menu (game)
#: kollisionui.rc:9
#, kde-format
msgid "&Game"
msgstr "&Jeu"

#: main.cpp:41
#, kde-format
msgid "Kollision"
msgstr "Kollision"

#: main.cpp:43
#, kde-format
msgid "Casual ball game"
msgstr "Jeu classique de ballons "

#: main.cpp:45
#, kde-format
msgid "(c) 2007 Paolo Capriotti"
msgstr "(c) 2007 Paolo Capriotti"

#: main.cpp:48
#, kde-format
msgid "Paolo Capriotti"
msgstr "Paolo Capriotti"

#: main.cpp:49
#, kde-format
msgid "Dmitry Suzdalev"
msgstr "Dmitry Suzdalev"

#: main.cpp:50
#, kde-format
msgid "Matteo Guarnieri"
msgstr "Matteo Guarnieri"

#: main.cpp:50
#, kde-format
msgid "Original idea"
msgstr "Idée originale"

#: main.cpp:51
#, kde-format
msgid "Brian Croom"
msgstr "Brian Croom"

#: main.cpp:51
#, kde-format
msgid "Port to use KGameRenderer"
msgstr "Portage pour utiliser « KGameRenderer »"

#: mainarea.cpp:76
#, kde-format
msgid ""
"Welcome to Kollision\n"
"Click to start a game"
msgstr ""
"Bienvenue dans Kollision\n"
"Cliquez pour démarrer une partie"

#: mainarea.cpp:209
#, kde-format
msgid ""
"Game paused\n"
"Click or press %1 to resume"
msgstr ""
"Jeu en pause\n"
"Cliquez ou appuyez sur %1 pour reprendre"

#: mainarea.cpp:271 mainarea.cpp:533
#, kde-format
msgid "%1 ball"
msgid_plural "%1 balls"
msgstr[0] "%1 balle"
msgstr[1] "%1 balles"

#: mainarea.cpp:541
#, kde-format
msgid ""
"GAME OVER\n"
"You survived for %1 second\n"
"Click to restart"
msgid_plural ""
"GAME OVER\n"
"You survived for %1 seconds\n"
"Click to restart"
msgstr[0] ""
"JEU TERMINÉ\n"
"Vous avez survécu %1 seconde\n"
"Cliquez pour redémarrer"
msgstr[1] ""
"JEU TERMINÉ\n"
"Vous avez survécu %1 secondes\n"
"Cliquez pour redémarrer"

#: mainwindow.cpp:81
#, kde-format
msgctxt "@action"
msgid "End Game"
msgstr "Terminer la partie"

#: mainwindow.cpp:90
#, kde-format
msgctxt "@option:check"
msgid "&Increase Ball Size"
msgstr "Augmenter la ta&ille des balles"

#: mainwindow.cpp:97
#, kde-format
msgctxt "@option:check"
msgid "Play Sounds"
msgstr "Jouer les sons"

#: mainwindow.cpp:138
#, kde-format
msgid "Balls: %1"
msgstr "Balles : %1"

#: mainwindow.cpp:143
#, kde-format
msgid "Time: %1 second"
msgid_plural "Time: %1 seconds"
msgstr[0] "Temps : %1 seconde"
msgstr[1] "Temps : %1 secondes"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_EnableSounds)
#: preferences.ui:16
#, kde-format
msgid "Enable &sounds"
msgstr "Activer les &sons"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_IncreaseBallSize)
#: preferences.ui:23
#, kde-format
msgid "Enable automatically &increasing ball size"
msgstr "Activer l'&augmentation automatique de la taille des balles"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: preferences.ui:32
#, kde-format
msgid "&Difficulty:"
msgstr "&Difficulté :"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_GameDifficulty)
#: preferences.ui:43
#, kde-format
msgctxt "Difficulty level"
msgid "Easy"
msgstr "Facile"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_GameDifficulty)
#: preferences.ui:48
#, kde-format
msgctxt "Difficulty level"
msgid "Medium"
msgstr "Moyenne"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_GameDifficulty)
#: preferences.ui:53
#, kde-format
msgctxt "Difficulty level"
msgid "Hard"
msgstr "Difficile"

#. i18n: ectx: property (text), widget (QLabel, label)
#: preferences.ui:65
#, kde-format
msgid "&Animation smoothness:"
msgstr "Fluidité des &animations :"

#~ msgid "KDE collision game"
#~ msgstr "Jeu de collisions pour KDE"

#~ msgid "Difficulty of the game."
#~ msgstr "Niveau de difficulté du jeu."

#~ msgid "Abort game"
#~ msgstr "Interrompre le jeu"

#~ msgid "4 balls"
#~ msgstr "4 balles"
